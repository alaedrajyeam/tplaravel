<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\module>
 */
class moduleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'CodeModules'=>fake()->numberBetween(1,205),
            'titre'=>fake()->title(),
            'MH'=>fake()->numberBetween(1,120)
        ];
    }
}
